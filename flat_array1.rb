class FlatArray

  def initialize(array)
    @array = array
  end


  def recursive_flatten
	  @result =[]
    for i in @array
  	  @result << i unless i.is_a? Array
      if i.is_a? Array
        for k in i 
          @result << k
        end
      end
    end

    @result.map{|i| i.map{|k| @result << k} if i.is_a? Array}
    @result.map{|i| @result.delete(i) if i.is_a? Array}
    pp @result
    return @result
  end

end

FlatArray.new([[1,2,[3]],4]).recursive_flatten
FlatArray.new([23,534,[345,1,0]]).recursive_flatten
